/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioPalabraPalindroma;

import java.util.Scanner;

/**
 *
 * @author Mario
 */
public class Palindromos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Palindromos p1 = new Palindromos();
        System.out.println ("DETERMINAR SI UNA PALABRA ES PALINDROMA");
        System.out.println ("Por favor introduzca una palabra:");
        String et = "";
        Scanner entradaEscaner = new Scanner (System.in);
        et = entradaEscaner.nextLine ();
        System.out.println("La Palabra Ingresada  "+p1.palindroma(et));

    }

    public String palindroma(String sPalabra){
        String mensaje="";
        int inc = 0;
        int des = sPalabra.length()-1;
        boolean bError = false;
        
        while ((inc<des) && (!bError)){
			
	if (sPalabra.charAt(inc)==sPalabra.charAt(des)){				
		inc++;
		des--;
	} else {
		bError = true;
	}
       }
        if (!bError)
	mensaje="ES UN PALINDROMO";
        else
	mensaje="NO ES UN PALINDROMO";
        
        return mensaje;
    
    }
   

}
